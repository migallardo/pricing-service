run:
	./gradlew bootRun

unit-test:
	./gradlew unitTest

int-test:
	./gradlew intTest

pitest:
	./gradlew pitest

test-all:
	./gradlew unitTest intTest pitest