## Pricing Service

### 1. Running the app
```./gradlew bootRun```
You can use the included Postman collection in this project or 
make your own request.

### 1. Running test

Unit, Intg and Mutation tests
```./gradlew unitTest intTest pitest```

Unit tests
```./gradlew unitTest```

Integration tests
```./gradlew intTest```

Mutation tests
```./gradlew pitest```
