package com.amaris.inditex.pricing.price.adapter.in.web;

import com.amaris.inditex.pricing.price.application.port.in.GetPricingUseCase;
import com.amaris.inditex.pricing.price.domain.PriceResponse;
import io.swagger.v3.oas.annotations.Operation;
import java.time.LocalDateTime;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pricing/")
@RequiredArgsConstructor
public class PricingRestController {

  private final GetPricingUseCase getPricingUseCase;

  @Operation(summary = "Get product PVP by brand and date")
  @GetMapping("brand/{brand}")
  @Valid
  public PriceResponse getPricing(
      @PathVariable Long brand,
      @RequestParam @NotNull Long product,
      @RequestParam @NotNull @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          LocalDateTime date) {
    return getPricingUseCase.getPricing(brand, product, date);
  }
}
