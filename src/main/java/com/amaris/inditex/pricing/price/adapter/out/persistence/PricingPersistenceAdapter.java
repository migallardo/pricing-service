package com.amaris.inditex.pricing.price.adapter.out.persistence;

import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.PriceEntity;
import com.amaris.inditex.pricing.price.adapter.out.persistence.mapper.PriceMapper;
import com.amaris.inditex.pricing.price.adapter.out.persistence.repository.PriceRepository;
import com.amaris.inditex.pricing.price.application.port.out.GetPricingPort;
import com.amaris.inditex.pricing.price.domain.PriceResponse;
import java.time.LocalDateTime;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class PricingPersistenceAdapter implements GetPricingPort {

  private final PriceRepository priceRepository;
  private final PriceMapper priceMapper;

  @Override
  public Optional<PriceResponse> getProductPriceByDateAndPriority(
      Long brandId, Long productId, LocalDateTime date) {
    Optional<PriceEntity> price =
        priceRepository.findProductPriceDetailsByBrandAndDate(brandId, productId, date);

    return price.map(priceMapper::mapToDomain);
  }
}
