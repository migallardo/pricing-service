package com.amaris.inditex.pricing.price.adapter.out.persistence.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "BRANDS")
public class BrandEntity implements Serializable {

  @Id @GeneratedValue private Long id;

  @Column(name = "NAME", length = 200, nullable = false)
  private String name;
}
