package com.amaris.inditex.pricing.price.adapter.out.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(
    name = "PRICES",
    indexes = {
      @Index(name = "brand_idx", columnList = "BRAND_ID"),
      @Index(name = "product_idx", columnList = "PRODUCT_ID"),
      @Index(name = "start_date_idx", columnList = "START_DATE"),
      @Index(name = "end_date_idx", columnList = "END_DATE")
    })
public class PriceEntity implements Serializable {
  @Id private Long id;

  @ManyToOne
  @JoinColumn(name = "BRAND_ID", nullable = false)
  private BrandEntity brand;

  @ManyToOne
  @JoinColumn(name = "PRODUCT_ID", nullable = false)
  private ProductEntity product;

  @Column(name = "START_DATE", nullable = false)
  private LocalDateTime startDate;

  @Column(name = "END_DATE", nullable = false)
  private LocalDateTime endDate;

  @Column(name = "PRICE_LIST", nullable = false)
  private Integer priceList;

  @Column(name = "PRIORITY", nullable = false)
  private Integer priority;

  @Column(name = "PRICE", nullable = false)
  private BigDecimal price;

  @Column(name = "CURRENCY", length = 3, nullable = false)
  private String currency;
}
