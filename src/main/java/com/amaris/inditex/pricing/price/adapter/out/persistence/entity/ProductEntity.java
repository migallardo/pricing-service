package com.amaris.inditex.pricing.price.adapter.out.persistence.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "PRODUCTS")
@SequenceGenerator(name = "PRODUCT_ID_GENERATOR", initialValue = 35455, allocationSize = 1)
public class ProductEntity implements Serializable {

  @Id
  @GeneratedValue(generator = "PRODUCT_ID_GENERATOR")
  private Long id;

  @Column(name = "NAME")
  private String name;
}
