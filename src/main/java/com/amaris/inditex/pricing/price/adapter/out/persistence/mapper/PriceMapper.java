package com.amaris.inditex.pricing.price.adapter.out.persistence.mapper;

import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.BrandEntity;
import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.PriceEntity;
import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.ProductEntity;
import com.amaris.inditex.pricing.price.domain.DateRange;
import com.amaris.inditex.pricing.price.domain.PriceResponse;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.stereotype.Component;

@Component
public class PriceMapper {

  public PriceResponse mapToDomain(PriceEntity entity) {
    return Optional.ofNullable(entity).map(this::buildPriceResponse).orElse(null);
  }

  private PriceResponse buildPriceResponse(PriceEntity entity) {
    return PriceResponse.builder()
        .pvp(entity.getPrice())
        .currency(entity.getCurrency())
        .appliedRate(entity.getPriceList())
        .brandId(Optional.ofNullable(entity.getBrand()).map(BrandEntity::getId).orElse(null))
        .productId(Optional.ofNullable(entity.getProduct()).map(ProductEntity::getId).orElse(null))
        .rateDateRange(buildDateRange(entity.getStartDate(), entity.getEndDate()))
        .build();
  }

  private DateRange buildDateRange(LocalDateTime start, LocalDateTime end) {
    return DateRange.builder().from(start).until(end).build();
  }
}
