package com.amaris.inditex.pricing.price.adapter.out.persistence.repository;

import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.PriceEntity;
import java.time.LocalDateTime;
import java.util.Optional;

public interface PriceRepository {

  Optional<PriceEntity> findProductPriceDetailsByBrandAndDate(
      Long brandId, Long productId, LocalDateTime date);
}
