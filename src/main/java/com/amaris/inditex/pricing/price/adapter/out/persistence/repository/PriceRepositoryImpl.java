package com.amaris.inditex.pricing.price.adapter.out.persistence.repository;

import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.PriceEntity;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
public class PriceRepositoryImpl implements PriceRepository {

  private final EntityManager entityManager;

  @Override
  public Optional<PriceEntity> findProductPriceDetailsByBrandAndDate(
      Long brandId, Long productId, LocalDateTime date) {
    CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    CriteriaQuery<PriceEntity> query = builder.createQuery(PriceEntity.class);
    Root<PriceEntity> price = query.from(PriceEntity.class);

    List<Predicate> predicates = new ArrayList<>();

    predicates.add(builder.equal(price.get("brand"), brandId));
    predicates.add(builder.equal(price.get("product"), productId));
    predicates.add(
        builder.between(builder.literal(date), price.get("startDate"), price.get("endDate")));
    query
        .where(predicates.toArray(new Predicate[NumberUtils.INTEGER_ZERO]))
        .orderBy(builder.desc(price.get("priority")));

    return entityManager
        .createQuery(query)
        .setMaxResults(NumberUtils.INTEGER_ONE)
        .getResultStream()
        .findFirst();
  }
}
