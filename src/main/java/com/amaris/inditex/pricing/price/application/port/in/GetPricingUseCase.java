package com.amaris.inditex.pricing.price.application.port.in;

import com.amaris.inditex.pricing.price.domain.PriceResponse;
import java.time.LocalDateTime;

public interface GetPricingUseCase {

  PriceResponse getPricing(Long brand, Long product, LocalDateTime date);
}
