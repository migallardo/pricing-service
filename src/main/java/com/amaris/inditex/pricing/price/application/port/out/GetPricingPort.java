package com.amaris.inditex.pricing.price.application.port.out;

import com.amaris.inditex.pricing.price.domain.PriceResponse;
import java.time.LocalDateTime;
import java.util.Optional;

public interface GetPricingPort {
  Optional<PriceResponse> getProductPriceByDateAndPriority(
      Long brandId, Long productId, LocalDateTime date);
}
