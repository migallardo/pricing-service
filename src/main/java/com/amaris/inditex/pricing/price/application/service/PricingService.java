package com.amaris.inditex.pricing.price.application.service;

import com.amaris.inditex.pricing.price.application.port.in.GetPricingUseCase;
import com.amaris.inditex.pricing.price.application.port.out.GetPricingPort;
import com.amaris.inditex.pricing.price.domain.PriceResponse;
import com.amaris.inditex.pricing.price.shared.exception.NotFoundException;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PricingService implements GetPricingUseCase {

  private final GetPricingPort getPricingPort;

  @Override
  public PriceResponse getPricing(Long brand, Long product, LocalDateTime date) {

    return getPricingPort
        .getProductPriceByDateAndPriority(brand, product, date)
        .orElseThrow(() -> new NotFoundException("Pricing not found for this product."));
  }
}
