package com.amaris.inditex.pricing.price.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DateRange implements Serializable {
  private LocalDateTime from;
  private LocalDateTime until;
}
