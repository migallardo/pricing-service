package com.amaris.inditex.pricing.price.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PriceResponse implements Serializable {
  private Long brandId;
  private Long productId;
  private Integer appliedRate;
  private DateRange rateDateRange;
  private BigDecimal pvp;
  private String currency;
}
