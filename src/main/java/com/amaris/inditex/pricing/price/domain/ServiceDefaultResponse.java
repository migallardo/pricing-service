package com.amaris.inditex.pricing.price.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ServiceDefaultResponse implements Serializable {

  @Builder.Default private int status = 200;
  private String code;
  private String message;
  @Builder.Default private LocalDateTime timestamp = LocalDateTime.now();
  @Builder.Default private String debugId = UUID.randomUUID().toString();
}
