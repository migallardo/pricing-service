package com.amaris.inditex.pricing.price.shared.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends ServiceRuntimeException {
  public BadRequestException(String message) {
    super(message, HttpStatus.BAD_REQUEST);
  }
}
