package com.amaris.inditex.pricing.price.shared.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends ServiceRuntimeException {

  public NotFoundException(String message) {
    super(message, HttpStatus.NOT_FOUND);
  }
}
