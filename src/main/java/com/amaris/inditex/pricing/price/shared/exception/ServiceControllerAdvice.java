package com.amaris.inditex.pricing.price.shared.exception;

import com.amaris.inditex.pricing.price.domain.ServiceDefaultResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Log4j2
public class ServiceControllerAdvice {

  @ExceptionHandler(NotFoundException.class)
  protected ResponseEntity<ServiceDefaultResponse> resolve(ServiceRuntimeException exception) {

    log.error(
        "Request failed - Reason=[{}] with message=[{}]",
        exception.httpStatus.getReasonPhrase(),
        exception.getMessage());
    return new ResponseEntity<>(buildDefaultResponse(exception), exception.getHttpStatus());
  }

  @ExceptionHandler(MissingServletRequestParameterException.class)
  protected ResponseEntity<ServiceDefaultResponse> resolve(
      MissingServletRequestParameterException exception) {
    ServiceRuntimeException ex = new BadRequestException(exception.getMessage());
    log.error(
        "Request failed - Reason=[{}] with message=[{}]",
        ex.httpStatus.getReasonPhrase(),
        ex.getMessage());
    return new ResponseEntity<>(buildDefaultResponse(ex), ex.getHttpStatus());
  }

  private ServiceDefaultResponse buildDefaultResponse(ServiceRuntimeException ex) {
    return ServiceDefaultResponse.builder()
        .status(ex.httpStatus.value())
        .code(ex.httpStatus.getReasonPhrase())
        .message(ex.getMessage())
        .build();
  }
}
