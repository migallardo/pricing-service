package com.amaris.inditex.pricing.price.shared.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ServiceRuntimeException extends RuntimeException {

  protected final HttpStatus httpStatus;

  public ServiceRuntimeException(String message, HttpStatus httpStatus) {
    super(message);
    this.httpStatus = httpStatus;
  }
}
