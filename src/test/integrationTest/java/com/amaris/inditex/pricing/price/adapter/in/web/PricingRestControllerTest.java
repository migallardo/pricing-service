package com.amaris.inditex.pricing.price.adapter.in.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.amaris.inditex.pricing.PricingApplication;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = PricingApplication.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureMockMvc(print = MockMvcPrint.LOG_DEBUG, printOnlyOnFailure = false)
public class PricingRestControllerTest {

  private static final String REQUEST_FORMAT_PATTERN = "%s%d?date=%s&product=%s";
  private static final String ENDPOINT_URL = "/pricing/brand/";
  private static final Long VALID_BRAND = NumberUtils.LONG_ONE;
  private static final Long VALID_PRODUCT = 35455L;

  List<Triple<LocalDateTime, String, Integer>> datesToTest = new ArrayList<>();

  @Autowired private MockMvc mockMvc;

  @Before
  public void setup() {
    if (!NumberUtils.INTEGER_ZERO.equals(datesToTest.size())) return;
    datesToTest.add(Triple.of(LocalDateTime.of(2020, Month.JUNE, 14, 10, 0, 0), "35.50", 1));
    datesToTest.add(Triple.of(LocalDateTime.of(2020, Month.JUNE, 14, 16, 0, 0), "25.45", 2));
    datesToTest.add(Triple.of(LocalDateTime.of(2020, Month.JUNE, 15, 10, 0, 0), "30.50", 3));
    datesToTest.add(Triple.of(LocalDateTime.of(2020, Month.JUNE, 16, 21, 0, 0), "38.95", 4));
  }

  @Test
  public void testValidRequestsPerDayAndHour() {
    datesToTest.forEach(
        t -> {
          try {
            testSuccessfulResponseByDayBrandAndProduct(
                dateToISO(t.getLeft()),
                status().isOk(),
                NumberUtils.createBigDecimal(t.getMiddle()),
                t.getRight());
          } catch (Exception e) {
            e.printStackTrace();
          }
        });
  }

  @Test
  public void testInvalidRequestWithEmptyDateValue() throws Exception {
    mockMvc
        .perform(
            get(
                String.format(
                    REQUEST_FORMAT_PATTERN,
                    ENDPOINT_URL,
                    VALID_BRAND,
                    StringUtils.EMPTY,
                    VALID_PRODUCT)))
        .andDo(MockMvcResultHandlers.print())
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.message").isNotEmpty());
  }

  @Test
  public void testShouldReturnNotFoundWhenProductDoesNotExist() throws Exception {
    mockMvc
        .perform(
            get(
                String.format(
                    REQUEST_FORMAT_PATTERN,
                    ENDPOINT_URL,
                    VALID_BRAND,
                    dateToISO(LocalDateTime.of(2020, Month.JUNE, 14, 10, 0, 0)),
                    999999)))
        .andDo(MockMvcResultHandlers.print())
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$.message").value("Pricing not found for this product."));
  }

  private void testSuccessfulResponseByDayBrandAndProduct(
      String date, ResultMatcher expectedHttpStatus, BigDecimal expectedPVP, int expectedPriceList)
      throws Exception {
    mockMvc
        .perform(
            get(
                String.format(
                    REQUEST_FORMAT_PATTERN, ENDPOINT_URL, VALID_BRAND, date, VALID_PRODUCT)))
        .andDo(MockMvcResultHandlers.print())
        .andExpect(expectedHttpStatus)
        .andExpect(jsonPath("$.brandId").isNotEmpty())
        .andExpect(jsonPath("$.productId").isNotEmpty())
        .andExpect(jsonPath("$.currency").isNotEmpty())
        .andExpect(jsonPath("$.rateDateRange.from").isNotEmpty())
        .andExpect(jsonPath("$.rateDateRange.until").isNotEmpty())
        .andExpect(jsonPath("$.pvp").value(expectedPVP.stripTrailingZeros()))
        .andExpect(jsonPath("$.appliedRate").value(expectedPriceList))
        .andExpect(jsonPath("$.brandId").isNotEmpty());
  }

  private String dateToISO(LocalDateTime toFormat) {
    DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
    return toFormat.format(formatter);
  }
}
