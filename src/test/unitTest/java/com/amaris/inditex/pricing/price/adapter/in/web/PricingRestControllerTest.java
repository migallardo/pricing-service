package com.amaris.inditex.pricing.price.adapter.in.web;

import com.amaris.inditex.pricing.price.application.port.in.GetPricingUseCase;
import com.amaris.inditex.pricing.price.domain.PriceResponse;
import java.time.LocalDateTime;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PricingRestControllerTest {

  @InjectMocks private PricingRestController controller;
  @Mock private GetPricingUseCase useCase;

  @Test
  public void testShouldValidateExpectedResponse() {
    Mockito.when(
            useCase.getPricing(
                ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong(),
                ArgumentMatchers.any(LocalDateTime.class)))
        .thenReturn(PriceResponse.builder().build());
    PriceResponse response =
        controller.getPricing(NumberUtils.LONG_ONE, NumberUtils.LONG_ONE, LocalDateTime.now());
    Assert.assertNotNull("Response shouldn't be null", response);
  }
}
