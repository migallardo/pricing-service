package com.amaris.inditex.pricing.price.adapter.out.persistence;

import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.PriceEntity;
import com.amaris.inditex.pricing.price.adapter.out.persistence.mapper.PriceMapper;
import com.amaris.inditex.pricing.price.adapter.out.persistence.repository.PriceRepository;
import com.amaris.inditex.pricing.price.domain.PriceResponse;
import com.amaris.inditex.pricing.price.util.TestUtil;
import java.time.LocalDateTime;
import java.util.Optional;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PricingPersistenceAdapterTest {
  @Mock private PriceRepository repository;
  @Mock private PriceMapper mapper;
  @InjectMocks private PricingPersistenceAdapter pricingPersistenceAdapter;

  @Test
  public void testShouldValidatePersistenceAdapter() {
    Mockito.when(
            repository.findProductPriceDetailsByBrandAndDate(
                ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong(),
                ArgumentMatchers.any(LocalDateTime.class)))
        .thenReturn(Optional.of(TestUtil.createPriceEntity()));

    Mockito.when(mapper.mapToDomain(ArgumentMatchers.any(PriceEntity.class))).thenCallRealMethod();

    Optional<PriceResponse> response =
        pricingPersistenceAdapter.getProductPriceByDateAndPriority(
            NumberUtils.LONG_ONE, NumberUtils.LONG_ONE, LocalDateTime.now());
    Assert.assertNotNull("Response shouldn't be null", response.orElse(null));
  }
}
