package com.amaris.inditex.pricing.price.adapter.out.persistence.mapper;

import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.PriceEntity;
import com.amaris.inditex.pricing.price.domain.PriceResponse;
import com.amaris.inditex.pricing.price.util.TestUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PriceMapperTest {

  @InjectMocks private PriceMapper mapper;

  @Test
  public void testShouldValidateMapToDomainIsCorrect() {

    PriceEntity entity = TestUtil.createPriceEntity();
    PriceResponse result = mapper.mapToDomain(entity);

    Assert.assertEquals(entity.getBrand().getId(), result.getBrandId());
    Assert.assertEquals(entity.getProduct().getId(), result.getProductId());
    Assert.assertEquals(entity.getPrice(), result.getPvp());
    Assert.assertEquals(entity.getCurrency(), result.getCurrency());
    Assert.assertEquals(entity.getPriceList(), result.getAppliedRate());
  }
}
