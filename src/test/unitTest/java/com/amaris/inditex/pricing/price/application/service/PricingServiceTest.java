package com.amaris.inditex.pricing.price.application.service;

import com.amaris.inditex.pricing.price.application.port.out.GetPricingPort;
import com.amaris.inditex.pricing.price.domain.PriceResponse;
import com.amaris.inditex.pricing.price.shared.exception.NotFoundException;
import java.time.LocalDateTime;
import java.util.Optional;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PricingServiceTest {

  @Mock private GetPricingPort getPricingPort;
  @InjectMocks private PricingService pricingService;

  @Test
  public void testShouldReturnDomainObject() {
    Mockito.when(
            getPricingPort.getProductPriceByDateAndPriority(
                ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong(),
                ArgumentMatchers.any(LocalDateTime.class)))
        .thenReturn(Optional.of(PriceResponse.builder().build()));

    PriceResponse response =
        pricingService.getPricing(NumberUtils.LONG_ONE, NumberUtils.LONG_ONE, LocalDateTime.now());

    Assert.assertNotNull("Response shouldn't be null", response);
  }

  @Test(expected = NotFoundException.class)
  public void testShouldThrowNotFoundExceptionWhenNoResultsForPrice() {
    Mockito.when(
            getPricingPort.getProductPriceByDateAndPriority(
                ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong(),
                ArgumentMatchers.any(LocalDateTime.class)))
        .thenReturn(Optional.empty());

    pricingService.getPricing(NumberUtils.LONG_ONE, NumberUtils.LONG_ONE, LocalDateTime.now());
  }
}
