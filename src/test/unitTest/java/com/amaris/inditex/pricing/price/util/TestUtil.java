package com.amaris.inditex.pricing.price.util;

import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.BrandEntity;
import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.PriceEntity;
import com.amaris.inditex.pricing.price.adapter.out.persistence.entity.ProductEntity;
import java.time.LocalDateTime;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.math.NumberUtils;

@UtilityClass
public class TestUtil {

  public static PriceEntity createPriceEntity() {
    return PriceEntity.builder()
        .brand(BrandEntity.builder().id(NumberUtils.LONG_ONE).build())
        .product(ProductEntity.builder().id(NumberUtils.LONG_ONE).build())
        .currency("EUR")
        .priceList(NumberUtils.INTEGER_ONE)
        .price(NumberUtils.createBigDecimal("25.95"))
        .startDate(LocalDateTime.now())
        .endDate(LocalDateTime.now())
        .build();
  }
}
